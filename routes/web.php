<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/notActivated', function () {
    return view('errors.notActivated');
});

Auth::routes();

Route::get('/', 'InstanceController@index');

Route::get('/instances', 'InstanceController@index')->name('instances.index');
Route::post('/instances', 'InstanceController@store')->name('instances.store');
Route::get('/instances/create', 'InstanceController@create')->name('instances.create');
Route::get('/instances/{id}', 'InstanceController@show')->name('instances.show');
Route::post('/instances/{id}', 'InstanceController@update')->name('instances.update');

Route::get('/users', 'UserController@index')->name('users.index');
Route::get('/users/{id}', 'UserController@show')->name('users.show');
Route::post('/users/{id}', 'UserController@update')->name('users.update');

Route::get('/settings', 'SettingController@show')->name('settings.show');
Route::post('/settings', 'SettingController@update')->name('settings.update');
