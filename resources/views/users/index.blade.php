@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Lista de Usuários
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <td>Status</td>
                  <td>Nome</td>
                  <td>E-mail</td>
                  <td>Data de Criação</td>
                </tr>
              </thead>
              <tbody>
                @foreach($users as $user)
                <tr>
                  <td style="font-weight: 700;">
                    @if ($user['activated'])
                      <span style="color: #5cb85c">
                        {{ 'Ativado' }}
                      </span>
                    @else
                      <span style="color: #d9534f">
                        {{ 'Bloqueado' }}
                      </span>
                    @endif
                  </td>
                  <td><a href="{{ route('users.show', $user->id) }}">{{ $user['name'] }}</a></td>
                  <td>{{ $user->email }}</td>
                  <td>{{ date("d/m/Y", strtotime($user['createdAt'])) }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <?php echo $users->render(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
