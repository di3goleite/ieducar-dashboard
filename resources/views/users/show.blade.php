@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Página do Usuário
        </div>
        <div class="panel-body">
          @if (Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
          @endif
          <form method="POST" action="{{ route('users.update', $user->id) }}">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Nome</label>
                  <input type="text" class="form-control" id="name" name="name" value="{{ $user['name'] }}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">Status</label>
                  <select name="activated" class="form-control">
                    @if ($user['activated'])
                      <option value="true" selected>Ativado</option>
                      <option value="false">Bloqueado</option>
                    @else
                      <option value="true">Ativado</option>
                      <option value="false" selected>Bloqueado</option>
                    @endif
                  </select>
                  <p class="form-error">{{ $errors->first('activated') }}</p>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Atualizar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
