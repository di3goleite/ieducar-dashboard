@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Nova instância do 123escola
        </div>
        <div class="panel-body">
          <section id="loading" class="loaders">
            <span class="loader loader-quart"></span> Gerando a Nova Instância...
          </section>
          <form id="form" method="POST" action="{{ route('instances.store') }}">
            {{ csrf_field() }}
            <div class="form-group">
              <label for="name">Nome Completo</label>
              <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
              <p class="form-error">{{ $errors->first('name') }}</p>
            </div>
            <div class="form-group">
              <label for="company-name">Razão Social</label>
              <input type="text" class="form-control" id="company-name" name="company-name" value="{{ old('company-name') }}">
              <p class="form-error">{{ $errors->first('company-name') }}</p>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
                  <p class="form-error">{{ $errors->first('email') }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone">Telefone</label>
                  <input type="text" class="form-control" id="phone" name="phone" value="{{ old('phone') }}">
                  <p class="form-error">{{ $errors->first('phone') }}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="identification-code">CPF/CNPJ</label>
                  <input type="text" class="form-control" id="identification-code" name="identification-code" value="{{ old('identification-code') }}">
                  <p class="form-error">{{ $errors->first('identification-code') }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="hiring-date">Data de Contratação</label>
                  <input type="date" class="form-control" id="hiring-date" name="hiring-date" value="{{ old('hiring-date') }}">
                  <p class="form-error">{{ $errors->first('hiring-date') }}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">Estado</label>
                  <select name="state" class="form-control">
                    <option value="AC">Acre</option>
                    <option value="AL">Alagoas</option>
                    <option value="AP">Amapá</option>
                    <option value="AM">Amazonas</option>
                    <option value="BA" selected>Bahia</option>
                    <option value="CE">Ceará</option>
                    <option value="DF">Distrito Federal</option>
                    <option value="ES">Espírito Santo</option>
                    <option value="GO">Goiás</option>
                    <option value="MA">Maranhão</option>
                    <option value="MT">Mato Grosso</option>
                    <option value="MS">Mato Grosso do Sul</option>
                    <option value="MG">Minas Gerais</option>
                    <option value="PA">Pará</option>
                    <option value="PB">Paraíba</option>
                    <option value="PR">Paraná</option>
                    <option value="PE">Pernambuco</option>
                    <option value="PI">Piauí</option>
                    <option value="RJ">Rio de Janeiro</option>
                    <option value="RN">Rio Grande do Norte</option>
                    <option value="RS">Rio Grande do Sul</option>
                    <option value="RO">Rondônia</option>
                    <option value="RR">Roraima</option>
                    <option value="SC">Santa Catarina</option>
                    <option value="SP">São Paulo</option>
                    <option value="SE">Sergipe</option>
                    <option value="TO">Tocantins</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="city">Cidade</label>
                  <input type="text" class="form-control" id="city" name="city" value="{{ old('city') }}">
                  <p class="form-error">{{ $errors->first('city') }}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="state-registration">Inscrição Estadual</label>
                  <input type="text" class="form-control" id="state-registration" name="state-registration" value="{{ old('state-registration') }}">
                  <p class="form-error">{{ $errors->first('state-registration') }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="municipal-registration">Inscrição Municipal</label>
                  <input type="text" class="form-control" id="municipal-registration" name="municipal-registration" value="{{ old('municipal-registration') }}">
                  <p class="form-error">{{ $errors->first('municipal-registration') }}</p>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary" onclick="return showLoading();">Criar Instância</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
