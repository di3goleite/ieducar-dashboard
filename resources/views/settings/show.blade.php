@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                  Configurações do sistema
                </div>
                <div class="panel-body">
                  @if (isset($message))
                    <div class="alert alert-success">{{ $message }}</div>
                  @endif
                  <form method="POST" action="{{ route('settings.update') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="backup-version">Versão do Backup</label>
                      <input type="text" class="form-control" id="backup-version" name="backup-version" value="{{ $settings['backup-version'] }}">
                      <p class="form-error">{{ $errors->first('backup-version') }}</p>
                    </div>
                    <div class="form-group">
                      <label for="backup-version">Pasta do 123escola</label>
                      <input type="text" class="form-control" id="instances-folder" name="instances-folder" value="{{ $settings['instances-folder'] }}">
                      <p class="form-error">{{ $errors->first('instances-folder') }}</p>
                    </div>
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
