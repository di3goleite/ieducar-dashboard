<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
  </head>
  <body>
    <div class="container">
      <div style="text-align: center; padding: 150px 250px;">
        <h1 style="color: red;">Usuário bloqueado!</h1>
        <h2>Entre em contato com o administrador do sistema para ativar a sua conta.</h2>
        <a href="{{ route('login') }}" class="btn btn-lg btn-primary">Voltar para o Login</a>
      </div>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/util.js') }}"></script>
  </body>
</html>
