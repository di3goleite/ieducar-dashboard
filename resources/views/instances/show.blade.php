@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Instância do 123escola
        </div>
        <div class="panel-body">
          @if (Session::has('message'))
            <div class="alert alert-success">{{ Session::get('message') }}</div>
          @endif
          <form method="POST" action="{{ route('instances.update', $instance->id) }}">
            {{ csrf_field() }}
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label">URL</label>
                  <p class="form-control" style="background-color: #eee;">
                    <a href="{{ 'http://'.$instance['endpoint'].'.'.$url }}" target="_blank">{{ 'http://'.$instance['endpoint'].'.'.$url }}</a>
                  </p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="hiring-date">Data de Contratação</label>
                  <input type="text" class="form-control" id="hiring-date" name="hiring-date" value="{{ date("d/m/Y", strtotime($instance['hiring-date'])) }}" disabled>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="name">Nome Completo</label>
                  <input type="text" class="form-control" id="name" name="name" value="{{ $instance['name'] }}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="company-name">Razão Social</label>
                  <input type="text" class="form-control" id="company-name" name="company-name" value="{{ $instance['company-name'] }}" disabled>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="state">Estado</label>
                  <input type="text" class="form-control" id="state" name="state" value="{{ $instance['state'] }}" disabled>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="city">Cidade</label>
                  <input type="text" class="form-control" id="city" name="city" value="{{ $instance['city'] }}" disabled>
                </div>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" class="form-control" id="email" name="email" value="{{ $instance['email'] }}">
                  <p class="form-error">{{ $errors->first('email') }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="phone">Telefone</label>
                  <input type="text" class="form-control" id="phone" name="phone" value="{{ $instance['phone'] }}">
                  <p class="form-error">{{ $errors->first('phone') }}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="state-registration">Inscrição Estadual</label>
                  <input type="text" class="form-control" id="state-registration" name="state-registration" value="{{ $instance['state-registration'] }}">
                  <p class="form-error">{{ $errors->first('state-registration') }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="municipal-registration">Inscrição Municipal</label>
                  <input type="text" class="form-control" id="municipal-registration" name="municipal-registration" value="{{ $instance['municipal-registration'] }}">
                  <p class="form-error">{{ $errors->first('municipal-registration') }}</p>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label for="identification-code">CPF/CNPJ</label>
                  <input type="text" class="form-control" id="identification-code" name="identification-code" value="{{ $instance['identification-code'] }}">
                  <p class="form-error">{{ $errors->first('identification-code') }}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="email">Status</label>
                  <select name="status" class="form-control">
                    @if ($instance['status'])
                      <option value="true" selected>Ativado</option>
                      <option value="false">Bloqueado</option>
                    @else
                      <option value="true">Ativado</option>
                      <option value="false" selected>Bloqueado</option>
                    @endif
                  </select>
                  <p class="form-error">{{ $errors->first('status') }}</p>
                </div>
              </div>
            </div>
            <button type="submit" class="btn btn-primary">Atualizar</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
