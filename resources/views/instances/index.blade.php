@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Lista de Instâncias
        </div>
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <tr>
                  <td>Status</td>
                  <td>Razão Social</td>
                  <td>Nome</td>
                  <td>URL</td>
                  <td>Contratação</td>
                </tr>
              </thead>
              <tbody>
                @foreach($instances as $instance)
                <tr>
                  <td style="font-weight: 700;">
                    @if ($instance['status'])
                      <span style="color: #5cb85c">
                        {{ 'Ativado' }}
                      </span>
                    @else
                      <span style="color: #d9534f">
                        {{ 'Bloqueado' }}
                      </span>
                    @endif
                  </td>
                  <td><a href="{{ route('instances.show', $instance->id) }}">{{ $instance['company-name'] }}</a></td>
                  <td>{{ $instance->name }}</td>
                  <td><a href="{{ 'http://'.$instance['endpoint'].'.'.$url }}" target="_blank">{{ 'http://'.$instance['endpoint'].'.'.$url }}</a></td>
                  <td>{{ date("d/m/Y", strtotime($instance['hiring-date'])) }}</td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          <?php echo $instances->render(); ?>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
