#!/bin/bash

# Before start update the repos
sudo apt-get update -y

# General porpuse
sudo apt-get install -y curl wget rpl unzip git

# PostgreSQL
sudo apt-get install -y libreadline6 libreadline6-dev make gcc zlib1g-dev flex bison postgresql postgresql-client

# PHP dependencies
sudo apt-get istall -y libapache2-mod-php5 php5-pgsql php5-curl php-pear

# Download pear packages
wget http://download.pear.php.net/package/Mail-1.2.0.tgz
wget http://download.pear.php.net/package/Net_Socket-1.0.14.tgz
wget http://download.pear.php.net/package/Net_SMTP-1.6.2.tgz
wget http://download.pear.php.net/package/Net_URL2-2.0.5.tgz
wget http://download.pear.php.net/package/HTTP_Request2-2.2.0.tgz
wget http://download.pear.php.net/package/Services_ReCaptcha-1.0.3.tgz

# Install pear packages
sudo pear install -O Mail-1.2.0.tgz
sudo pear install -O Net_Socket-1.0.14.tgz
sudo pear install -O Net_SMTP-1.6.2.tgz
sudo pear install -O Net_URL2-2.0.5.tgz
sudo pear install -O HTTP_Request2-2.2.0.tgz
sudo pear install -O Services_ReCaptcha-1.0.3.tgz

# Delete temp pear packages
rm Mail-1.2.0.tgz
rm Net_Socket-1.0.14.tgz
rm Net_SMTP-1.6.2.tgz
rm Net_URL2-2.0.5.tgz
rm HTTP_Request2-2.2.0.tgz
rm Services_ReCaptcha-1.0.3.tgz

# Initialize PostgreSQL
/etc/init.d/postgresql start
