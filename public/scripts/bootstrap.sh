#! /bin/bash

# Usage: sh bootstrap.sh FOLDER_PATH
# 1 - FOLDER_PATH="/var/www/123escola.com.br"

FOLDER_PATH=$1

# Brazil states array
declare -a arr=("ac" "al" "ap" "am" "ba" "ce" "df" "es" "go" "ma" "mt" "ms" "mg" "pa" "pb" "pr" "pe" "pi" "rj" "rn" "rs" "ro" "rr" "sc" "sp" "se" "to")

for i in "${arr[@]}"
do
   mkdir $FOLDER_PATH/$i
done
