#! /bin/bash

# Usage: sh new.sh NAME STATE DB_DUMP BASE_DIR
# 1 - NAME="diego"
# 2 - STATE="ba"
# 3 - DB_DUMP="ieducar_atualizado_20170422.backup"
# 4 - BASE_DIR="/var/www/123escola.com.br"
NAME=$1
STATE=$2
DOMAIN="123escola.com.br"

DB_NAME=$2-$1
DB_DUMP=$3
DB_USER="forge"
DB_PASS="forge"

APP_DIR=$4/$STATE/$NAME
DUMP_PATH=$4/$3
APP_DIR_TEMP=$APP_DIR"-temp"

# Download db dump and move
echo "- download db dump"
wget -O $DUMP_PATH https://s3-us-west-2.amazonaws.com/portabilis2/public/ieducar/$DB_DUMP
bash -c "pg_restore -f $DUMP_PATH.sql $DUMP_PATH"

# Replace i-Educar occurrences by Razao
rpl "i-Educar" "Razao" $DUMP_PATH.sql

# Create db
echo "- create db"
bash -c "createdb $DB_NAME"

# Restore db dump
echo "- restore db dump"
bash -c "psql -d $DB_NAME -f $DUMP_PATH.sql"

# Define search path
echo "- define search path"
bash -c "psql -d $DB_NAME -c 'ALTER DATABASE \"$DB_NAME\" SET search_path = '$user' public, portal, cadastro, acesso, alimentos, consistenciacao, historico, pmiacoes, pmicontrolesis, pmidrh, pmieducar, pmiotopic, urbano, modules;'"

# Clean directory
echo "- delete dump"
rm $DUMP_PATH
rm $DUMP_PATH.sql

# Clone the latest version
echo "- clone repo"
git -c http.sslVerify=false clone http://softwarepublico.gov.br/gitlab/i-educar/i-educar.git $APP_DIR

# Reconfigure application settings
echo "- replace rules"
rpl "app.database.dbname   = ieducar" "app.database.dbname   = $DB_NAME" $APP_DIR/ieducar/configuration/ieducar.ini
rpl "app.database.username = postgres" "app.database.username = $DB_USER" $APP_DIR/ieducar/configuration/ieducar.ini
rpl "app.database.password = postgres" "app.database.password = $DB_PASS" $APP_DIR/ieducar/configuration/ieducar.ini
rpl "        name: ieducar" "        name: $DB_NAME" $APP_DIR/phinx.yml
rpl "        user: postgres" "        user: $DB_USER" $APP_DIR/phinx.yml
rpl "        pass: postgres" "        pass: $DB_PASS" $APP_DIR/phinx.yml

# Go to APP_DIR
echo "- APP_DIR Clean up"
cd $4

# Go to current phinx bin and excute
echo "- phinx migrate"
$APP_DIR/ieducar/vendor/bin/phinx migrate

# APP DIR Clean up
mv $APP_DIR $APP_DIR_TEMP
mv $APP_DIR_TEMP/ieducar $APP_DIR
rm -rf $APP_DIR_TEMP

# Update virtualhost file
echo "- Update virtualhost file"
echo -e "\n<VirtualHost *:80>\n\tServerName $DB_NAME.$DOMAIN\n\tServerAlias www.$DB_NAME.$DOMAIN\n\tDocumentRoot $APP_DIR\n\t<Directory "$APP_DIR">\n\t\tOrder deny,allow\n\t\tAllowOverride All\n\t</Directory>\n</VirtualHost>" >> /etc/apache2/sites-available/000-default.conf

echo "- Restart apache2"
reload_server

# Add ieducar logo to login page
rpl '    <div id="corpo">' '    <div id="corpo" style="margin-top: 120px;"><img src="http://i.imgur.com/8YKeGG4.png" style="float: right; margin-right: 30px; margin-bottom: 20px;" height="243">' $APP_DIR/intranet/templates/nvp_htmlloginintranet.tpl
rpl '      <div id="service-info">' '<!--       <div id="service-info">' $APP_DIR/intranet/templates/nvp_htmlloginintranet.tpl
rpl '      <div class="clear"></div>' '      <div class="clear"></div> -->' $APP_DIR/intranet/templates/nvp_htmlloginintranet.tpl
rpl 'i-Educar' 'Razão' $APP_DIR/intranet/topo.php

# Go back to base server dir
echo "- finish"
