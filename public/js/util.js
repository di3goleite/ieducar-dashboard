(function($) {
  $("#form").submit(function(e){
    e.preventDefault();
    $('#loading').css('display', 'block');
    this.submit();
  });
})(jQuery);
