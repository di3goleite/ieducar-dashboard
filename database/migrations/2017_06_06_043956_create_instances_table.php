<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstancesTable extends Migration
{
  /**
   * Run the migrations.
   *
   * @return void
   */
  public function up()
  {
    Schema::create('instances', function (Blueprint $table) {
      $table->increments('id');
      $table->string('name');
      $table->string('company-name');
      $table->string('email');
      $table->string('phone');
      $table->string('identification-code');
      $table->dateTime('hiring-date');
      $table->string('state');
      $table->string('city');
      $table->string('state-registration');
      $table->string('municipal-registration');
      $table->string('endpoint');
      $table->softDeletes();
      $table->timestamps();
    });
  }

  /**
   * Reverse the migrations.
   *
   * @return void
   */
  public function down()
  {
    Schema::dropIfExists('instances');
  }
}
