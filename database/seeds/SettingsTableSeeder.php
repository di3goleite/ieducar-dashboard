<?php

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'backup-version' => 'ieducar_atualizado_20170422.backup',
            'instances-folder' => '/var/www/123escola.com.br'
        ]);
    }
}
