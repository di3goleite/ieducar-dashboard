# 123escola Dashboard

See: https://bitbucket.org/di3goleite/ieducar-landing-page

Install server dependencies:

```
sudo apt-get install php7.0 php7.0-pgsql php7.0-json php7.0-curl php7.0-mcrypt php7.0-gd postgresql phppgadmin mysql-server libapache2-mod-auth-mysql php7.0-mysql build-essential libxml2-dev phppgadmin php7.0-mbstring php7.0-dom unzip
```

Install project dependencies:

```
./public/scripts/dependencies.sh
```

Move this project to: `/var/www/admin.123escola.com.br`:

```
mv ieducar-dashboard /var/www/admin.123escola.com.br
```

Clone i-educar landing page project:

```
git clone https://bitbucket.org/di3goleite/ieducar-landing-page /var/www/123escola.com.br
```

Run the Bootstrap Script (You can find in `/var/www/admin.123escola.com.br`):

```
./public/scripts/bootstrap.sh /var/www/123escola.com.br

```

Give all permissions to `/var/www/123escola.com.br` folder:

```
chmod -R 777 /var/www/123escola.com.br
```

Create ieducar database

```
su - postgres
createdb ieducar
CREATE ROLE forge WITH SUPERUSER LOGIN PASSWORD 'forge';
```

Create a new `.env` file inside the project folder using `.env.example` template. The needed changes are:

```
APP_ENV=production
MAIL_USERNAME=example@email.com
MAIL_PASSWORD=123example
```

To make it work with Apache 2 is important to create a `www-data` role with a database with the same name:

```
su - postgres
createdb www-data
psql
CREATE ROLE "www-data" WITH SUPERUSER LOGIN;
```

Configure `000-default.conf` Apache 2 VirtualHost:

1 - Open: `/etc/apache2/sites-available/000-default.conf`

```
<VirtualHost *:80>
	ServerAdmin diegojleite@gmail.com
	ServerName avisoseleisonline.com.br
	ServerAlias www.avisoseleisonline.com.br

	RewriteEngine On
	RewriteCond %{HTTP:CF-Visitor} '"scheme":"http"'
	RewriteRule ^(.*)$ https://avisoseleisonline.com.br$1 [L]

	DocumentRoot /var/www/avisoseleisonline.com.br/public_html
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<VirtualHost *:80>
	ServerAdmin diegojleite@gmail.com
	ServerName 123escola.com.br
	ServerAlias www.123escola.com.br

	RewriteEngine On
	RewriteCond %{HTTP:CF-Visitor} '"scheme":"http"'
	RewriteRule ^(.*)$ https://123escola.com.br$1 [L]

	DocumentRoot /var/www/123escola.com.br
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>

<VirtualHost *:80>
	ServerAdmin diegojleite@gmail.com
	ServerName admin.123escola.com.br
	ServerAlias www.admin.123escola.com.br
	DocumentRoot /var/www/admin.123escola.com.br/public
	ErrorLog ${APACHE_LOG_DIR}/error.log
	CustomLog ${APACHE_LOG_DIR}/access.log combined
	<Directory "/var/www/admin.123escola.com.br/public">
		AllowOverride All
	</Directory>
</VirtualHost>
```

2 - Reload Apache: `service apache2 reload`

3 - Restart Apache: `service apache2 restart`

Compile and Enable Reload Server Executable

```
gcc reload_server.c -o reload_server
chown root reload_server
chmod u=rwx,go=xr,+s reload_server
mv reload_server /usr/bin/reload_server
```

Run Laravel Migrations (Inside project root folder):

```
php artisan migration
```

Run Laravel Seeds:

```
php artisan db:seed
```
