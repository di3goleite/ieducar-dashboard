<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Instance extends Model
{
  /**
   * The database table used by the model.
   *
   * @var string
   */
  protected $table = 'instances';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name',
    'company-name',
    'email',
    'phone',
    'identification-code',
    'hiring-date',
    'state',
    'city',
    'state-registration',
    'municipal-registration',
    'endpoint'
  ];

  /**
   * Generated date fields.
   *
   * @var array
   */
  protected $dates = [
    'deleted_at'
  ];
}
