<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Instance;
use App\Setting;

class InstanceController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  private function sanitize($string) {
    $sanitizedString = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
    $sanitizedString = preg_replace('/[^a-zA-Z0-9\ ]/', '', $sanitizedString);
    $sanitizedString = preg_replace('/\ /', '-', $sanitizedString);

    return strtolower($sanitizedString);
  }

  private function getUrl() {
    return env('APP_ENV') === 'production' ? '123escola.com.br' : '127.0.0.1:8000';
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $url = $this->getUrl();
    $instances = Instance::latest()->paginate(10);
    return view('instances.index', compact('instances', 'url'));
  }

  public function show($id)
  {
    $url = $this->getUrl();
    $instance = Instance::findOrFail($id);
    return view('instances.show', compact('instance', 'url'));
  }

  public function create()
  {
    return view('instances.create');
  }

  public function store(Request $request)
  {
    $data = $request->all();

    $companyName = $this->sanitize($data['company-name']);
    $state = strtolower($data['state']);

    $endpoint = $state . '-' . $companyName;
    $data['endpoint'] = $endpoint;
    $data['status'] = true;

    $validator = Validator::make($data, [
      'name' => 'required',
      'company-name' => 'required',
      'email' => 'required',
      'phone' => 'required',
      'identification-code' => 'required',
      'hiring-date' => 'required',
      'state' => 'required',
      'city' => 'required',
      'state-registration' => 'required',
      'municipal-registration' => 'required',
      'endpoint' => 'required|unique:instances',
      'status' => 'required|boolean'
    ]);

    if($validator->fails()) {
      return redirect()
        ->route('instances.create')
        ->withErrors($validator)
        ->withInput();
    } else {
      unset($data['_token']);
      $instance = Instance::create($data);
      $settings = Setting::first();

      exec($_SERVER['DOCUMENT_ROOT'] . '/scripts/new.sh' . ' ' . $companyName . ' ' . $state . ' ' . $settings['backup-version'] . ' ' . $settings['instances-folder']);
      $message = 'Nova Instância criada com sucesso!';

      return redirect()->route('instances.show', ['id' => $instance->id])->with('message', $message);
    }
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $data['status'] = $data['status'] === 'true' ? true : false;

    $validator = Validator::make($data, [
      'email' => 'required',
      'phone' => 'required',
      'identification-code' => 'required',
      'state-registration' => 'required',
      'municipal-registration' => 'required',
      'status' => 'required|boolean'
    ]);

    if($validator->fails()) {
      return redirect()
        ->route('instances.show', ['id' => $id])
        ->withErrors($validator)
        ->withInput();
    } else {
      unset($data['_token']);

      $instance = Instance::findOrFail($id);
      $settings = Setting::first();

      $companyName = $this->sanitize($instance['company-name']);
      $state = strtolower($instance['state']);

      // Bloqueou
      if($data['status'] === false && $instance['status'] === true) {
        $data['temp-dir'] = uniqid($companyName . '-');

        exec($_SERVER['DOCUMENT_ROOT'] . '/scripts/move.sh' . ' ' . $settings['instances-folder'].'/'.$state.'/'.$companyName . ' ' . $settings['instances-folder'].'/'.$state.'/'.$data['temp-dir']);
      //Ativou
      } elseif($data['status'] === true && $instance['status'] === false) {
        $data['temp-dir'] = NULL;

        exec($_SERVER['DOCUMENT_ROOT'] . '/scripts/move.sh' . ' ' . $settings['instances-folder'].'/'.$state.'/'.$instance['temp-dir'] . ' ' . $settings['instances-folder'].'/'.$state.'/'.$companyName);
      // Nada
      } else {
        $data['temp-dir'] = NULL;
      }

      $instance['email'] = $data['email'];
      $instance['phone'] = $data['phone'];
      $instance['status'] = $data['status'];
      $instance['temp-dir'] = $data['temp-dir'];
      $instance['identification-code'] = $data['identification-code'];
      $instance['state-registration'] = $data['state-registration'];
      $instance['municipal-registration'] = $data['municipal-registration'];
      $instance->save();

      return redirect()
        ->route('instances.show', ['id' => $instance->id])
        ->with('message', 'Dados atualizados com sucesso!');
    }
  }
}
