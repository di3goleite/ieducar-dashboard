<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\User;

class UserController extends Controller
{
  /**
   * Create a new controller controller.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {
    $users = User::latest()->paginate(10);
    return view('users.index', compact('users'));
  }

  public function show($id)
  {
    $user = User::findOrFail($id);
    return view('users.show', compact('user', 'url'));
  }

  public function update(Request $request, $id)
  {
    $data = $request->all();
    $data['activated'] = $data['activated'] === 'true' ? true : false;

    $validator = Validator::make($data, [
      'activated' => 'required|boolean'
    ]);

    if($validator->fails()) {
      return redirect()
        ->route('users.show', ['id' => $id])
        ->withErrors($validator)
        ->withInput();
    } elseif($id == 1) {
      return redirect()
        ->route('users.show', ['id' => $id])
        ->with('message', 'O super administrador não pode ser editado!');
    } else {
      unset($data['_token']);

      $user = User::findOrFail($id);
      $user['activated'] = $data['activated'];
      $user->save();

      return redirect()
        ->route('users.show', ['id' => $user->id])
        ->with('message', 'Dados atualizados com sucesso!');
    }
  }
}
