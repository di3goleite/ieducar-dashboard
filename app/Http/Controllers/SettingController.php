<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use App\Setting;

class SettingController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {
    $this->middleware('auth');
  }

  public function show()
  {
    $settings = Setting::find(1);
    return view('settings.show', compact('settings'));
  }

  public function update(Request $request)
  {
    $data = $request->all();

    $validator = Validator::make($data, [
      'backup-version' => 'required',
      'instances-folder' => 'required',
    ]);

    if($validator->fails()) {
      return redirect('/settings')
        ->withErrors($validator)
        ->withInput();
    } else {
      $settings = Setting::firstOrNew(['id' => 1]);
      $settings['backup-version'] = $data['backup-version'];
      $settings['instances-folder'] = $data['instances-folder'];
      $settings->save();

      $message = 'Dados atualizados com sucesso!';
      return view('settings.show', compact('settings', 'message'));
    }
  }
}
